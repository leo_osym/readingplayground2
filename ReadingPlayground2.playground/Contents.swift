import UIKit

// type aliases
typealias AudioSample = UInt16
var asam: AudioSample = 23
print(asam)

//tuples
let x = ("Call","me",23,[3,4,5])
var xx:(name: String, age: Int, salary: Double) = ("John",32,21400.6)
print(xx)
xx = (name:"Axel",age:26,salary:13353.56)
print("Person \(xx.name) has salary \(xx.salary)")
let (person, age, salary) = xx
print(age)
// swap inside tuple
var (x1,x2) = (23,54)
print(x1,x2)
(x2,x1)=(x1,x2)
print(x1,x2)
// string
let myString = "!✓😛"
for char in myString.unicodeScalars{
    print(char)
}
// sets
var bands:Set<String> = ["Nightwish", "The Gathering", "Delain"]
bands.insert("Delain")
print(bands) //can't insert the same values
bands.insert("Within Temptation")
print(bands)
var bands2:Set<String> = ["Nightwish", "Amaranthe", "Lacuna Coil"]
print("After substracting: \(bands2.subtracting(bands))")
print("After union: \(bands2.union(bands))")
print("After intersection: \(bands2.intersection(bands))")
// conditional statements
var somePoint = (1, 1)
switch somePoint {
case (0, 0):
    print("\(somePoint) is at the origin")
case (_, 0):
    print("\(somePoint) is on the x-axis")
case (0, _):
    print("\(somePoint) is on the y-axis")
case (-2...2, -2...2):
    print("\(somePoint) is inside the box")
default:
    print("\(somePoint) is outside of the box")
}
// value binding
let anotherPoint = (2, 3)
switch anotherPoint {
case (let x, 0):
    print("on the x-axis with an x value of \(x)")
case (0, let y):
    print("on the y-axis with a y value of \(y)")
case let (x, y):
    print("somewhere else at (\(x), \(y))")
}
// where
let person2 = ("Axel", 26)
switch person2 {
case let (xx, yy) where xx == "Axel":
    print("Axel is \(yy)")
    fallthrough
case let (xx, yy) where yy == 26:
    print("\(xx) is \(yy)")
default: print("default")
}
// some functions
func computeFunction(array: [Int]) -> (min: Int, max: Int, average: Int) {
    var min = 0, ave = 0, max = 0
    for num in array{
        if num < min{
            min = num
        }
        if num > max{
            max = num
        }
        ave += num
    }
    return (min:min,max:max, average:ave/array.count)
}
print(computeFunction(array: [2,3,5,1,0,8,-3,-2,5]))

func getMedian(in array: [Int]) -> Int? {
    var newArray = array
    if newArray.count>0{
        newArray.sort()
        return newArray[Int(newArray.count/2)]
    }
    return nil
}
// function type
var mathFunction: ([Int]) -> Int? = getMedian
if let median = mathFunction([3,1,0,8,9,5,3,5]){
    print("Median: \(median)")
}
// closures
let names = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
var reversedNames = names.sorted(by: { (s1: String, s2: String) -> Bool in return s1 > s2 } )
print(reversedNames)
reversedNames = names.sorted(by: { s1, s2 in s1 > s2 } )
print(reversedNames)
reversedNames = names.sorted() { $0 > $1 }
print(reversedNames)
print("------")
// closure with map
let digitNames = [
    0: "Zero", 1: "One", 2: "Two",   3: "Three", 4: "Four",
    5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine"
]
let numbers = [16, 58, 510]
let strings = numbers.map { (number) -> String in
    var number = number
    var output = ""
    repeat {
        output = digitNames[number % 10]! + output
        number /= 10
    } while number > 0
    return output
}
print(strings)
// capturing values
func makeIncrementer(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementer() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementer
}
var incrementBy = makeIncrementer(forIncrement: 12)
print(incrementBy())

// enums
enum CompassPoint: CaseIterable {
    case north
    case south
    case east
    case west
}
for dir in CompassPoint.allCases {
    print(dir)
}
var direction = CompassPoint.west
direction = .east

switch direction {
case .north:
    print("Lots of planets have a north")
case .south:
    print("Watch out for penguins")
case .east:
    print("Where the sun rises")
case .west:
    print("Where the skies are blue")
}

enum chooseCountry {
    case byName(String)
    case byBarCode(Int)
}
var country = chooseCountry.byName("Ukraine")
country = .byBarCode(482)
switch country {
case .byBarCode(let barCode):
    print("Country (bar code): \(barCode)")
case .byName(let name):
    print("Country: \(name).")
}

// recursive enums
indirect enum ArithmeticExpression {
    case number(Int)
    case addition(ArithmeticExpression, ArithmeticExpression)
}
var sum = ArithmeticExpression.addition(ArithmeticExpression.number(4), ArithmeticExpression.number(5))

// properties
class MyClass{
    private var num = 0
    public var Num: Int{
        get{
            return num
        }
        set(newValue) {
            if(newValue >= 0){
                num = newValue
            }
        }
    }
}
var myClass = MyClass()
print(myClass.Num)
myClass.Num = 5
print(myClass.Num)
myClass.Num = -10
print(myClass.Num)

struct MyStruct {
    var x = 0
    mutating func changeX(x deltaX: Int) {
        self = MyStruct(x: deltaX)
    }
}
var STR = MyStruct()
STR.changeX(x: 14)
print(STR.x)
print("-------")

class SomeStuff{
    private var array = [String]()
    public var Count = Int();
    subscript(index: Int) -> String? {
        get {
            if index < array.count {
                return array[index]
            }
            return nil
        }
        set {
            if newValue != nil && index < array.count{
                array.insert(newValue!, at: index)
                Count += 1
            }
        }
    }
    init(){
        Count = array.count
        array = ["come","wander","with","me"]
    }
}

var smStuff = SomeStuff()
print(smStuff[0])
print(smStuff[4])
smStuff[10] = "away" // no way
smStuff[3] = "away"
print(smStuff[3])

// inheritance of props
class Vehicle {
    var description: String {
        return "This is a vehicle"
    }
}
class Car: Vehicle {
    var gear = 4
    override var description: String {
        return super.description + " in gear \(gear)"
    }
}
var car = Car()
print(car.description)
//initializers inheritance
class Food {
    var name: String
    init(name: String) {
        self.name = name
    }
    convenience init() {
        self.init(name: "[Unnamed]")
    }
}
var food = Food()
print(food.name)
food = Food(name: "Bacon")
print(food.name)
//failable init
enum TemperatureUnit {
    case kelvin, celsius, fahrenheit
    init?(symbol: Character) {
        switch symbol {
        case "K":
            self = .kelvin
        case "C":
            self = .celsius
        case "F":
            self = .fahrenheit
        default:
            return nil
        }
    }
}
let fahrenheitUnit = TemperatureUnit(symbol: "F")
print(fahrenheitUnit!)

// optional chaining
class Person {
    var residence: Residence?
}
class Residence {
    var numberOfRooms = 1
}
var john = Person()
var res = Residence()
res.numberOfRooms = 4
john.residence = res
if let roomCount = john.residence?.numberOfRooms {
    print("John's residence has \(roomCount) room(s).")
} else {
    print("Unable to retrieve the number of rooms.")
}

var testScores = ["Axel": [86, 82, 84], "Leo": [79, 94, 81]]
testScores["Axel"]?[0] = 91
testScores["Leo"]?[0] += 1
testScores["Brian"]?[0] = 72
print(testScores)
// extensions
extension Int {
    func repetitions(task: () -> Void) {
        for _ in 0..<self {
            task()
        }
    }
}
3.repetitions {
    print("Hello!")
}
// protocols
// used only with classes
protocol FullyNamed: AnyObject {
    var fullName: String { get }
    func yearOfBirth() -> Int
}
class NewPerson: FullyNamed {
    var fullName: String
    var age: Int
    func yearOfBirth() -> Int {
        return (Calendar.current.component(.year, from: Date()) - age)
    }
    init(name: String, age: Int){
        fullName = name
        self.age = age
    }
}
var Axel = NewPerson(name: "Axel Primeiro", age: 26)
print(Axel.fullName)
print(Axel.yearOfBirth())

protocol SomeProtocol {
    var personInfo: String { get }
}
// extension implements protocol
extension NewPerson: SomeProtocol {
    var personInfo: String {
        return "\(fullName), \(age) years old"
    }
}
print(Axel.personInfo)


